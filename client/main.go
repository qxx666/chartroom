package main

import (
	"fmt"
)

var userId int
var userPwd string

func main() {
	var key int
	var loop = true

	for loop {
		fmt.Println("--------------欢迎登入聊天系统----------------")
		fmt.Println("\t\t\t 1 登入聊天室")
		fmt.Println("\t\t\t 2 注册用户")
		fmt.Println("\t\t\t 3 退出系统")
		fmt.Println("\t\t\t 请选择（1-3):")

		fmt.Scanf("%d", &key)
		switch key {
			case 1:
				fmt.Println("登入聊天室")
				loop = false
			case 2:
				fmt.Println("注册用户")
				loop = false
			case 3:
				fmt.Println("退出系统")
				loop = false
			default:
				fmt.Println("输入有误，请重新输入")
		}
	}
	//根据用户的输入，显示提示信息
	if key == 1 {
		fmt.Println("请输入用户的ID:")
		fmt.Scan(&userId)
		fmt.Println("请输入用户密码:")
		fmt.Scan(&userPwd)

		//先把登入的函数，写到另一个文件。
		err := login(userId, userPwd)
		if err!=nil{
			fmt.Printf("登入失败")

		}else {
			fmt.Printf("登入成功")
		}
	} else if key==2{
		fmt.Printf("进行用户注册逻辑")

	}

}
